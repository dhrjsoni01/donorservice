import dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';

import {logger} from './util/logger';

let PORT:number = 8000
if (process.env.PORT){
    PORT = Number.parseFloat(process.env.PORT)
}
const HOST:string = process.env.HOST || "0.0.0.0"



const app = express()


//morgan
app.use(morgan('dev'))

//bodyParser
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.get("/", (req,res,next)=>{
    res.json("Hello Doners..").status(200).end;
})


app.listen(PORT, HOST, ()=>{
    
    logger.info(`Service is running on port: ${PORT} and host ${HOST}`);
 
})

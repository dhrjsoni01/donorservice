import { model, Schema } from "mongoose";

export enum DonorStatus {
    ACTIVE = "active",
    REST_PERIOD = "resting",
    RETIRED = "retired/opted-out",
    DEACTIVE = "deactive"
}

export interface Donor {
    userId: string
    lastDonation: Date
    status: DonorStatus
    BloodGroup: string
    comment: string
}

const DonorSchema = new Schema<Donor>({
    userId:             { type: String, required: true },
    lastDonationDate:   { type: Date},
    status:             { type: String, required: true },
    BloodGroup:         { type: String, require: true },
    comment:            { type: String }
})

export const DonorModel = model<Donor>("Donor", DonorSchema);
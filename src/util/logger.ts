import winston, { transports } from "winston";
const { combine, timestamp, label, printf } = winston.format;


const LOG_DIR = process.env.LOG_DIR;

const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${level}] : ${message}`;
  });

export const logger = winston.createLogger({
    level:'debug',
    format:combine(
        timestamp(),
        myFormat
    ),
    // defaultMeta: {service: "Donor service"},
    transports:[
        new winston.transports.File({ filename: `${LOG_DIR}/error.log`, level: 'error' }),
        new winston.transports.File({ filename: `${LOG_DIR}/combined.log`}),
    ]
})

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
      format: winston.format.simple(),
    }));
  }